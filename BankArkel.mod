<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="BankArkel" version="1.0.1" date="08/27/2009">
        <Author name="Arkel Anfall &amp; Schlegler" email="mail@oberst.ch" />
        <Description text="You can look at the bank of every char everywhere" />
        <Dependencies>
	    	    <Dependency name="EA_UiDebugTools" />
            <Dependency name="EA_SettingsWindow" />
		        <Dependency name="EA_BackpackWindow" optional="false"	forceEnable="true"/>
         	  <Dependency name="EA_ChatSystem" />
            <Dependency name="EASystem_Utils" />
        </Dependencies>
        <Files>
           	<File name="BankArkel.xml" />
           	<File name="BankArkel.lua" />
        </Files>

        <SavedVariables>
            <SavedVariable name="BankArkel.db" global="true"/>
        </SavedVariables>
 
        <OnInitialize>
            <CallFunction name="BankArkel.Init" />
        </OnInitialize>
        <OnUpdate />

        <OnShutdown />
    </UiMod>
</ModuleFile>