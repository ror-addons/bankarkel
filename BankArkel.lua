BankArkel={}

local db={}

local WSTS=WStringToString
local STWS=StringToWString

local DBver=2

local database={	["version"]=DBver,
					["Entry"]={}
	}
local dbEntry=	{	["Name"]="",
					["lvl"]=0,
					["Ruf"]=0,
					["Tot"]={0,0,0,0},
					["Free"]={0,0,0,0},
					["Money"]=0,
					["Backpack"]={},
					["Bank"]={},
					["Equip"]={},
					["Player"]={["activeTitle"]=0,["area"]="",["zone"]=0,["career"]={},["race"]={},["Renown"]={},["Stats"]={},["armorValue"]=0}
					
		}
			
local NoBank=true	
local OldIndexBank=0
local OldIndexPack=0
local BP_Open=false
local BankIsReal=0

local IsPackWin=0
local ToolTipTable={}
------------------------------------------------------------------------------------------------------------------------- HOOKS
local BackPackShowHook
local BackPackHideHook
local BankCloseHook
local BankLBHook
local BankRBHook
local ChatKeyHook
------------------------------------------------------------------------------------------------------------------------- TEXTVARIABELN
local errTxt1="BankArkel Daten nicht gefunden! Auf default zur�cksetzen."
local errBtn1="OK"

local convTxt="converting data "

local plAttr={"St�rke","Ballistik"}
-------------------------------------------------------------------------------------------------------------------------
local InitTxt="BankArkel 1.01 initialized"

local tbOpen="�ffne"

local tbRuf="Ruf: "
local tbChnge="wechsle zu "
local tbPockets={}

local tbSelf="eigene"
local tbNone="keine"
local busy=false
local DataUpdated=false




------------------------------------------------------------------------------------------------------------------------------------
local PackIndex=1
local PackPocket=1
local PackName=""
local PlayerIndex=1
local PlayerName=1

------------------------------------------------------------------------------------------------------------------------------------DB CONVERSIONS
function BankArkel.DB1to2()
	local e=1
	BankArkel.db.version=2
 	while BankArkel.db.Entry[e] do
		BankArkel.db.Entry[e].Free={0,0,0,0} 	
		BankArkel.db.Entry[e].Player={0,"",0,{},{},{},{},0}
		e=e+1
	end
end


function BankArkel.ConvertDB()
	if(BankArkel.db) then
		if(BankArkel.db.version==1) then
			DialogManager.MakeOneButtonDialog(StringToWString(convTxt.."v.1 >>> v.2"),StringToWString(errBtn1),nil)
			BankArkel.DB1to2()
		end
	end
end

------------------------------------------------------------------------------------------------------------------------------------INIT ALL
function BankArkel.Init()

	
	BankArkel.SetupLanguage()
	BankArkel.ConvertDB()

	----------------------------------------------------------------------------------- HOOKS
	BackPackShowHook=EA_Window_Backpack.OnShown
    EA_Window_Backpack.OnShown=BankArkel.BackPackShow

	BackPackHideHook=EA_Window_Backpack.OnHidden
	EA_Window_Backpack.OnHidden=BankArkel.BackPackHide
	
	BankCloseHook=BankWindow.Hide
    BankWindow.Hide=BankArkel.OnHideBank

	BankLBHook=BankWindow.EquipmentLButtonDown
	BankWindow.EquipmentLButtonDown=BankArkel.BankLButtonDown

	BankRBHook=BankWindow.EquipmentRButtonDown
	BankWindow.EquipmentRButtonDown=BankArkel.BankRButtonDown

	ChatKeyHook=EA_ChatWindow.OnKeyEnter
	EA_ChatWindow.OnKeyEnter=BankArkel.ChkSlash


        ----------------------------------------------------------------------------------- EVENTS
	RegisterEventHandler(SystemData.Events.INTERACT_OPEN_BANK, "BankArkel.OnOpenBank")
	RegisterEventHandler(SystemData.Events.LOG_OUT, "BankArkel.LogOut")

	----------------------------------------------------------------------------------- GET CHARS NAME
	PlayerName=BankArkel.GetPlayerName()

	----------------------------------------------------------------------------------- CREATE WINDOWS

	CreateWindow("BankArkelBackpack",false)
	LabelSetText("BankArkelBackpackLabel", StringToWString(tbOpen))
	WindowSetShowing("BankArkelBackpack",false);
	WindowSetLayer("BankArkelBackpack",4)

	BankArkel.CreatePackWin()
	

	d(InitTxt)	
	EA_ChatWindow.Print(StringToWString(InitTxt))

	----------------------------------------------------------------------------------- CHECK DB
	if not BankArkel.db then
		DialogManager.MakeOneButtonDialog(StringToWString(errTxt1),StringToWString(errBtn1),BankArkel.ResetDB)
	else
		BankArkel.UpdateData()
		BankArkel.SetupCombos()
		--------------------------------------------------------------------------- OUTPUT INIT COMPLETE
	end
	
	----------------------------------------------------------TESTI
end

function BankArkel.PackTab()
	local id=WindowGetId(SystemData.MouseOverWindow.name)
	PackPocket=id
	BankArkel.PackShow(PackIndex,id)
end


function BankArkel.PackTabMover()
	local id=WindowGetId(SystemData.MouseOverWindow.name)
	Tooltips.CreateTwoLineActionTooltip(STWS(tbChnge..tbPockets[id]),PackName, SystemData.MouseOverWindow.name,Tooltips.ANCHOR_WINDOW_RIGHT )
end


function BankArkel.LogOut()

	BankArkel.UpdateData()
end


function BankArkel.CreatePackWin()

	CreateWindow("PackWin",false)
	WindowSetShowing("PackWin",false)
	local x,y
	local i=1

------------------------------------------------------------------Maximal 80 items
	for y=1,10 do
		for x=1,8 do
			CreateWindowFromTemplate("PackIcon"..i,"PackIconTmp","PackWin")
			WindowSetShowing("PackIcon"..i,false);
			WindowClearAnchors("PackIcon"..i)
 			WindowAddAnchor("PackIcon"..i, "topleft", "PackWin", "topleft",-29+x*48,80+y*48)
			WindowSetId("PackIcon"..i,i)
			i=i+1
		end
	end 
	isPackWin=false
end

function BankArkel.SetCharInfo(indx)
	if(not BankArkel.db.Entry[indx].Player or not BankArkel.db.Entry[indx].Player.area) then return end
	d("SetCharInfo: "..indx)
	local str=""
	local transl={1,8,9,3,7,6,3,5 }
	local plTit
	if(BankArkel.db.Entry[indx].Player.activeTitle~=0)then
		plTit=TomeGetPlayerTitleData(BankArkel.db.Entry[indx].Player.activeTitle)
	else
		plTit=TomeGetPlayerTitleData(1)
		plTit.name=towstring("")
	end
	
	str=WSTS(BankArkel.db.Entry[indx].Player.area.name).." ("..WSTS(GetZoneName(BankArkel.db.Entry[indx].Player.zone))..")"
	LabelSetText("PackWinPosi",STWS(str))

	str=string.format(WSTS(BankArkel.db.Entry[indx].Player.career.name)..", "..WSTS(BankArkel.db.Entry[indx].Player.race.name)..WSTS(plTit.name).."\n\n")
	str=str..string.format("Level: "..BankArkel.db.Entry[indx].lvl..", "..tbRuf..BankArkel.db.Entry[indx].Ruf..", "..WSTS(BankArkel.db.Entry[indx].Player.Renown.curTitle).."\n\n")
	for i=1,8 do
	d(i)
			str=str..string.format(BankArkel.db.Entry[indx].Player.Stats[transl[i]].baseValue.."\t"..plAttr[i].."\n")
	end
	str=str..string.format(BankArkel.db.Entry[indx].Player.armorValue.."\t"..plAttr[9].."\n\n")
	
	LabelSetText("PackWinCharInfo",STWS(str))
	WindowSetShowing("PackWinCharInfo",true)
	WindowSetShowing("PackWinPosi",true)
end

function BankArkel.PackImg(pocket)
	if( pocket>0 and pocket<7 ) then
		
		if(pocket==6) then		WindowSetShowing("PackWinCBKGR",true)
		else					WindowSetShowing("PackWinCBKGR",false)
		end
		
		for i=1,6 do 
			if(i==pocket) then
				WindowSetShowing("PackWinImg"..i,true)
				WindowSetShowing("PackWinTab"..i,false)
			else
				WindowSetShowing("PackWinImg"..i,false)
				WindowSetShowing("PackWinTab"..i,true)				
			end
		end	
	end
end


function BankArkel.PackShow(index,pocket)
	
	local texture, x, y
	local iNr
	local i
	local cntr
	local Anz,St,End
	
	local indx
	local packcntr=1

	local tIndx={ 75,77,78,79,76,41,49,65,17,25,73,74,33,57,0,0,16,24,32,40,48,56,64,72,80 }
	local Anz,x,y,lines

	-------------------------------------------------------------------------------------------------------	
	St=0			
	End=pocket-1	
	if(pocket<5) then	
		Anz=BankArkel.db.Entry[index].Free[pocket]+BankArkel.db.Entry[index].Tot[pocket]
		for i=1,End do St=St+BankArkel.db.Entry[index].Tot[i] end	
		End=St+BankArkel.db.Entry[index].Tot[pocket] St=St+1
	else
		Anz=80
	end
	
	if(pocket==4) then	
		d("pocket4: "..math.ceil(BankArkel.db.Entry[index].Tot[pocket]/8).." lines")	
		lines=math.ceil(BankArkel.db.Entry[index].Tot[pocket]/8) 
	elseif((Anz % 8)~=0 ) then	
		lines=math.ceil(Anz/8)
	else					
		lines=math.floor(Anz/8)	
	end
	
	
	-------------------------------------------------------------------------------------------------------

--	d("PSindex; "..index.."  PSpocket: "..pocket)
	
	PackIndex=index
	PackPocket=pocket
	
	BankArkel.PackHide()
	BankArkel.PackImg(pocket)
	
	----------------------------------------------------------------Kacheln
	if(pocket<6) then
		i=1
		for y=1,lines do
			for x=1,8 do
				WindowSetShowing("PackIcon"..i,true)	
				i=i+1
			end
		end
	else
		
	end
	----------------------------------------------------------------fill
	if(pocket==6) then	---------------------------------------CHAR
		BankArkel.SetCharInfo(PackIndex)
		i=1
		if(BankArkel.db.Entry[index].Equip) then
			while(BankArkel.db.Entry[index].Equip[i])do
				if(BankArkel.db.Entry[index].Equip[i].iconNum>0 and tIndx[i]>0) then
					ToolTipTable[tIndx[i]]=i
					texture,x,y=GetIconData(BankArkel.db.Entry[index].Equip[i].iconNum)
					DynamicImageSetTexture( "PackIcon"..tIndx[i].."Icon",texture,x,y )
					WindowSetShowing("PackIcon"..tIndx[i],true)
				end
				i=i+1
				
			end
		end
	elseif (pocket==5) then
		for i=1,80 do
			if(BankArkel.db.Entry[index] and BankArkel.db.Entry[index].Bank[i]) then
				texture,x,y=GetIconData(BankArkel.db.Entry[index].Bank[i].iconNum) 
				DynamicImageSetTexture( "PackIcon"..i.."Icon",texture,x,y )
				if(BankArkel.db.Entry[index].Bank[i].stackCount>1)then ButtonSetText("PackIcon"..i,towstring(BankArkel.db.Entry[index].Bank[i].stackCount))end
			end
		end
	else
		iNr=1
		for i=St,End do
			if(BankArkel.db.Entry[index] and BankArkel.db.Entry[index].Backpack[i]) then
				texture,x,y=GetIconData(BankArkel.db.Entry[index].Backpack[i].iconNum) 
				DynamicImageSetTexture( "PackIcon"..iNr.."Icon",texture,x,y )
				if(BankArkel.db.Entry[index].Backpack[i].stackCount>1) then ButtonSetText("PackIcon"..iNr,towstring(BankArkel.db.Entry[index].Backpack[i].stackCount))end
				iNr=iNr+1
			end
			
		end
	end	
	
	----------------------------------------------------------------Header

	WindowSetDimensions("PackWin",420,150+lines*48) 		
	WindowSetShowing("PackWin",true)
	WindowClearAnchors("PackWin")
 	WindowAddAnchor("PackWin", "topright", "EA_Window_Backpack", "topleft",30,0)
	local LblTxt=WSTS(BankArkel.db.Entry[index].Name).."'s  "..tbPockets[PackPocket]
	
	LabelSetText("PackWinTitleBarText",STWS(LblTxt))
	MoneyFrame.FormatMoney ("PackWinMoney",BankArkel.db.Entry[index].Money, MoneyFrame.SHOW_EMPTY_WINDOWS);
end


function BankArkel.PackHide( )
	local i

	for i=1,80 do 
		WindowSetShowing("PackIcon"..i,false) 
		DynamicImageSetTexture( "PackIcon"..i.."Icon","",0,0 ) 
		ButtonSetText("PackIcon"..i,STWS(""))
	end
	WindowSetShowing("PackWin",false)
	WindowSetShowing("PackWinCharInfo",false)
	WindowSetShowing("PackWinPosi",false)
end


function BankArkel.PackClose()
	ComboBoxSetSelectedMenuItem("BankArkelBackpackCombo",1)
	BankArkel.PackHide()
end


------------------------------------------------------------------------------------------------------------------------------------UPDATE DES BACKPACK
function BankArkel.UpdateData()

	PlayerName=BankArkel.GetPlayerName()
	dbEntry.Name=PlayerName
	dbEntry.Tot={0,0,0,0}
	dbEntry.Free={0,0,0,0}
	dbEntry.Money=GameData.Player.money
	dbEntry.Backpack={}
	dbEntry.lvl=GameData.Player.level
	dbEntry.Ruf=GameData.Player.Renown.curRank
	dbEntry.Equip=GetEquipmentData()

	dbEntry.Player.activeTitle=GameData.Player.activeTitle
	dbEntry.Player.area=GameData.Player.area
	dbEntry.Player.zone=GameData.Player.zone
	dbEntry.Player.career=GameData.Player.career
	dbEntry.Player.race=GameData.Player.race
	dbEntry.Player.Renown=GameData.Player.Renown
	dbEntry.Player.Stats=GameData.Player.Stats
	dbEntry.Player.armorValue=GameData.Player.armorValue
	
	local Trophy=GetTrophyData()	
	for i=1,5 do table.insert(dbEntry.Equip,Trophy[i])  end
	
	
	local Read={2,4,3,1}	--Equip,Craft,Medals,Quest	
	local i=1
	local cntr=1

	for i=1,4 do
		local indx=1
		local bpt=EA_Window_Backpack.GetItemsFromBackpack(Read[i])
		while(bpt[indx]~=nil) do
			if(bpt[indx].uniqueID~=0) then
				dbEntry.Tot[i]=dbEntry.Tot[i]+1
				dbEntry.Backpack[cntr]=bpt[indx]
				cntr=cntr+1
			else
				dbEntry.Free[i]=dbEntry.Free[i]+1
			end
			indx=indx+1
		end
	end
	
	PlayerIndex=BankArkel.GetIndexFromName(WSTS(PlayerName))
	
	if(PlayerIndex>0) then
		d("Adding at: "..PlayerIndex)
		if(BankArkel.db.Entry[PlayerIndex].Bank~=nil) then dbEntry.Bank=DataUtils.CopyTable(BankArkel.db.Entry[PlayerIndex].Bank)end
		BankArkel.db.Entry[PlayerIndex]=DataUtils.CopyTable(dbEntry)
	else
		dbEntry.Bank={}
		table.insert(BankArkel.db.Entry,dbEntry)
		PlayerIndex=BankArkel.GetIndex()
		d("CreatingNew at: "..PlayerIndex)
	end
end









function BankArkel.PackIconOver()

	local id=WindowGetId(SystemData.MouseOverWindow.name)
	local End
	local indx

	if(not BankArkel.db.Entry[PackIndex])then return end
	if(PackPocket==6) then
		if(ToolTipTable[id]) then
			local item=BankArkel.db.Entry[PackIndex].Equip[ToolTipTable[id]]
			Tooltips.CreateItemTooltip(item,SystemData.MouseOverWindow.name, Tooltips.ANCHOR_WINDOW_RIGHT) 	
		end
	elseif(PackPocket==5)then
		if(BankArkel.db.Entry[PackIndex].Bank[id]) then
			local item=BankArkel.db.Entry[PackIndex].Bank[id]
			Tooltips.CreateItemTooltip(item,SystemData.MouseOverWindow.name, Tooltips.ANCHOR_WINDOW_RIGHT) 	
		end
	else
		if(id>BankArkel.db.Entry[PackIndex].Tot[PackPocket]) then return end
		End=PackPocket-1
		indx=id
		for i=1,End do indx=indx+BankArkel.db.Entry[PackIndex].Tot[i] end
	
		if(BankArkel.db.Entry[PackIndex].Backpack[indx]) then
			local item=BankArkel.db.Entry[PackIndex].Backpack[indx]
			Tooltips.CreateItemTooltip(item,SystemData.MouseOverWindow.name, Tooltips.ANCHOR_WINDOW_RIGHT) 
		end
	end
end

function BankArkel.BankRButtonDown(...)
	--d(BankIsReal)
	if BankIsReal==1 then
		BankRBHook(...)	
	end
end

function BankArkel.BankLButtonDown(...)
	if BankIsReal==1 then
		BankLBHook(...)	
	end
end


function BankArkel.PackCombo(nr)

	if(nr~=nil and nr>0) then
		PackName=ComboBoxGetSelectedText("BankArkelBackpackCombo")
		if(WSTS(PackName)==tbSelf) then
			PackIndex=PlayerIndex
			PackPocket=1
			d("Self: "..PackIndex)
			BankArkel.PackShow(PackIndex,PackPocket)
		elseif(WSTS(PackName)==tbNone) then
			BankArkel.PackHide()
		else
			PackIndex=BankArkel.GetIndexFromName(WSTS(PackName))
			d("Other: "..PackIndex)
			BankArkel.PackShow(PackIndex,PackPocket)
		end
	end
end

------------------------------------------------------------------------------------------------------------------------------------SHOWING THE COMBOS
function BankArkel.HasBankItems(indx)
	if(BankArkel.db.Entry[indx].Bank) then
		for i=1,80 do
			if(BankArkel.db.Entry[indx].Bank[i] and BankArkel.db.Entry[indx].Bank[i].uniqueID~=0) then
				return true
			end
		end
	end
	return false
end

function BankArkel.SetupCombos()

	PlayerName=BankArkel.GetPlayerName()

	ComboBoxClearMenuItems("BankArkelBackpackCombo")
	ComboBoxAddMenuItem("BankArkelBackpackCombo",StringToWString(tbNone))
	ComboBoxAddMenuItem("BankArkelBackpackCombo",StringToWString(tbSelf))
	local i=BankArkel.GetIndex()

	i=1
	while (BankArkel.db.Entry[i]) do
		if(BankArkel.db.Entry[i].Name~=PlayerName) then
			ComboBoxAddMenuItem("BankArkelBackpackCombo",BankArkel.db.Entry[i].Name)
		end
		i=i+1
	end

	ComboBoxSetSelectedMenuItem("BankArkelBackpackCombo",1)
	OldIndexBank=1
	OldIndexPack=1
end



------------------------------------------------------------------------------------------------------------------------------------SHOWING THE COMBOS
function BankArkel.BackPackHide(...)
	BackPackHideHook(...)
	WindowSetShowing("BankArkelBackpack",false);
	BP_Open=false
	BankArkel.CloseBank()
	BankArkel.PackHide()
end


------------------------------------------------------------------------------------------------------------------------------------SHOWING THE COMBOS
function BankArkel.BackPackShow(...)

	if(not DataUpdated)then BankArkel.UpdateData() DataUpdated=true end
	BackPackShowHook(...)

	WindowClearAnchors("BankArkelBackpack")
 	WindowAddAnchor("BankArkelBackpack", "top", "EA_Window_Backpack", "bottom",-50,10)

	WindowSetShowing("BankArkelBackpack",true);

	BP_Open=true
end


------------------------------------------------------------------------------------------------------------------------------------SHOWSBANK
function BankArkel.ShowBank(index)
	if(BankArkel.db.Entry[index].Bank==nil)then  return 0 end
	BankWindow.Show()
	BankWindow.UpdateAllBankSlots( BankArkel.db.Entry[index].Bank )

end

------------------------------------------------------------------------------------------------------------------------------------SHOWSBACKPACK


function BankArkel.CloseBank()
		BankWindow.Hide()
		BankIsReal=0
end


------------------------------------------------------------------------------------------------------------------------------------UPDATE DER BANK
function BankArkel.UpdateBank()

	if(not DataUpdated)then BankArkel.UpdateData() DataUpdated=true end

	local index=BankArkel.GetIndex()
	if(BankIsReal~=0) then
		d("Saving Bank at "..index)
		if(index>0) then	---------------------------------------yes
			BankArkel.db.Entry[index].Bank=BankWindow.items
			BankArkel.SetupCombos()
		else			---------------------------------------no
			d("UPDATEBANK-ERROR: NOENTRY FOUND")
		end
	else
		d("Not saving bank")
	end

end

------------------------------------------------------------------------------------------------------------------------------------OPEN DER BANK (regul)
function BankArkel.OnOpenBank(...)
	BankIsReal=1
end


------------------------------------------------------------------------------------------------------------------------------------UPDATE DER BANK
function BankArkel.OnHideBank(...)
	if(BankIsReal~=0) then BankArkel.UpdateBank() end
	BankIsReal=0
	BankCloseHook(...)
end

------------------------------------------------------------------------------------------------------------------------------------GET CHARACTERS DB-INDEX
function BankArkel.GetIndex()
	local i=1
	while(BankArkel.db.Entry[i]~=nil) do
		if(BankArkel.db.Entry[i].Name==PlayerName) then return i else i=i+1 end
	end
	return 0
end

function BankArkel.GetIndexFromName(n)
	local i=1
	local WSname=STWS(n)

	while(BankArkel.db.Entry[i]~=nil) do
		if(BankArkel.db.Entry[i].Name==WSname) then return i else i=i+1 end
	end
	return 0
end




function BankArkel.GetPlayerName()
	return GameData.Account.CharacterSlot[GameData.Account.SelectedCharacterSlot].Name
end



------------------------------------------------------------------------------------------------------------------------------------ERSTELLEN DER DB
function BankArkel.ResetDB()
	BankArkel.db=database
	BankArkel.UpdateData()
	BankArkel.SetupCombos()
	PlayerIndex=BankArkel.GetIndex()
end

------------------------------------------------------------------------------------------------------------------------------------SPRACHEINSTELLUNG
function BankArkel.SetupLanguage()

	local Language=SystemData.Settings.Language.active

	-----------------------------------------------------------------------------------------------------------------DEUTSCH
	if(Language==3) then

		errTxt1="BankArkel: Daten nicht gefunden! Auf default zur�cksetzen."
		errBtn1="OK"
		
		tbOpen="�ffne"
		
		tbChnge="wechsle zu "
		tbPockets={"Tasche","Handwerkstasche","Zahlungsmittel","Questitems","Bank","Charakteransicht"}	
		plAttr={"St�rke","Ballistik","Intelligenz","Widerstand","Kampfgeschick","Initiative","Willenskraft","Leben","R�stung"}
		
		tbSelf="deine"
		tbNone="keine"
		tbRuf="Ruf: "
		convTxt="Konvertiere Daten "
	-----------------------------------------------------------------------------------------------------------------FRANCAIS
	elseif(Language==2) then

		errTxt1="BankArkel: Ne trouve pas les dates sauv�s! Remettre � l'�tat initial."
		errBtn1="OK"
		plAttr={"Force","Cap.de tir","Intelligence","Endurance","Cap.de combat","Initiative","Vollont�","Points vie","Armure"}
		tbPockets={"objets","objets d'artisanat","devises","objets de qu�te","banque","personnage"}	
		tbChnge="change a "

		tbOpen="aprir"
		tbSelf="le tien"
		tbNone="aucune"
		tbRuf="Rep: "
		convTxt="convertir les dates "
	-----------------------------------------------------------------------------------------------------------------ENGLISH
	else
		errTxt1="BankArkel: Can't open database! Reset to default."
		plAttr={"Strength","Ballistic","Intelligence","Toughness","Weapon Skill","Initiative","Willpower","Wounds","Armor"}
		tbPockets={"inventory","crafting items","currency","quest items","bank","char view"}	
		tbChnge="change to "
		errBtn1="OK"
		tbOpen="open"
		tbSelf="yours"
		tbNone="none"
		tbRuf="Rep: "
		convTxt="converting data "
	end
end




function BankArkel.Dump()
local e=1
local i=1

	d("______BA-DUMP______________________")
	d("db.ver: "..BankArkel.db.version)

	while BankArkel.db.Entry[e] do
		i=1
		d("____________________________________")
		d("")
		d("")
		d("Char "..e..": "..WSTS(BankArkel.db.Entry[e].Name))
		d("bag:________________________________")
		if(BankArkel.db.Entry[e].Backpack[1]) then
			while BankArkel.db.Entry[e].Backpack[i]~=nil do
				d(e.."A  "..BankArkel.db.Entry[e].Backpack[i].stackCount.." x "..WSTS(BankArkel.db.Entry[e].Backpack[i].name))
				i=i+1
			end
		else
			d("A  No BackPackDataFound")
		end
		d("bank:_______________________________")
		if(BankArkel.db.Entry[e].Bank) then
			for i=1,80 do
				if(BankArkel.db.Entry[e].Bank[i]) then
					if(BankArkel.db.Entry[e].Bank[i].uniqueID~=0) then
						d(e.."B  "..BankArkel.db.Entry[e].Bank[i].stackCount.." x "..WSTS(BankArkel.db.Entry[e].Bank[i].name))
					end
				end
			end
		else
			d("B  No BankDataFound")	
		end
		d("equip:_______________________________")
		i=1
		if(BankArkel.db.Entry[e].Equip) then
			while(BankArkel.db.Entry[e].Equip[i]) do
					if(BankArkel.db.Entry[e].Equip[i].equipSlot>0)then d(e.."C  "..WSTS(BankArkel.db.Entry[e].Equip[i].name)) end
					i=i+1
			end
		else
			d("B  No Equipment found")	
		end
		e=e+1
	end
		
	
end

function BankArkel.ChkSlash(...)

	local strng=tostring(EA_TextEntryGroupEntryBoxTextInput.Text)
	local i=1
	local toServe=0

	if string.find (strng,"/")==1 then 
		
		local txtSub=string.upper(string.sub (strng,2))

		if(string.match(txtSub,"BA_TEST")~=nil) then
			PackIndex=PlayerIndex
			BankArkel.PackShow(PackIndex,1)
			EA_TextEntryGroupEntryBoxTextInput.Text = L""
			ChatKeyHook(...)
		elseif(string.match(txtSub,"BA_DUMP")~=nil) then
			BankArkel.Dump()
			EA_TextEntryGroupEntryBoxTextInput.Text = L"/debug"
			ChatKeyHook(...)
		elseif(string.match(txtSub,"BA_RESETALL")~=nil) then
			BankArkel.ResetDB()
			BankArkel.SetupCombos()
			EA_TextEntryGroupEntryBoxTextInput.Text = L""
			ChatKeyHook(...)

		end
		
	end
	ChatKeyHook(...)	
		
end
